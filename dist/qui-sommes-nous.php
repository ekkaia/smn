<!DOCTYPE html>
<!--
███████╗ █████╗ ███████╗███████╗    ███╗   ███╗██╗ ██████╗ ██████╗  █████╗ ███╗   ██╗████████╗███████╗
██╔════╝██╔══██╗██╔════╝██╔════╝    ████╗ ████║██║██╔════╝ ██╔══██╗██╔══██╗████╗  ██║╚══██╔══╝██╔════╝
███████╗███████║█████╗  █████╗█████╗██╔████╔██║██║██║  ███╗██████╔╝███████║██╔██╗ ██║   ██║   ███████╗
╚════██║██╔══██║██╔══╝  ██╔══╝╚════╝██║╚██╔╝██║██║██║   ██║██╔══██╗██╔══██║██║╚██╗██║   ██║   ╚════██║
███████║██║  ██║██║     ███████╗    ██║ ╚═╝ ██║██║╚██████╔╝██║  ██║██║  ██║██║ ╚████║   ██║   ███████║
╚══════╝╚═╝  ╚═╝╚═╝     ╚══════╝    ╚═╝     ╚═╝╚═╝ ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═══╝   ╚═╝   ╚══════╝
-->
<html lang="fr" prefix="og: http://ogp.me/ns#">
  <head>
    <meta property="og:type" content="website">
    <title>Qui sommes-nous ? — Safe Migrants Nantes</title>
    <link rel="stylesheet" href="stylesheet/style.css">
    <link rel="icon" type="image/png" href="./images/favicon.png">
    <meta charset="UTF-8">
    <meta name="theme-color" content="#f79f24">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- OGP-->
    <meta name="description" content="Découvrez l'histoire et toutes les informations sur l'association Safe Migrants Nantes depuis sa création.">
    <meta property="og:site_name" content="Safe Migrants Nantes"><?php $url = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" ?>
    <meta property="og:url" content="<?= $url ?>">
    <meta property="og:title" content="Qui sommes-nous ?">
    <meta property="og:description" content="Découvrez l'histoire et toutes les informations sur l'association Safe Migrants Nantes depuis sa création.">
    <meta property="og:image" content="https://safe-migrants-nantes.org/images/logo.jpg">
    <meta property="og:locale" content="fr_FR">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
  </head>
  <body>
    <!-- Navigation-->
    <header id="header">
      <div><a class="header__link" href="https://safe-migrants-nantes.org"><img class="header__logo" src="https://safe-migrants-nantes.org/images/logo.svg" alt="Logo de Safe Migrants Nantes">
          <p class="header__title highlight--primary">SAFE MIGRANTS NANTES</p></a></div>
      <input id="header__checkbox" type="checkbox">
      <label id="header__checkbox--label" for="header__checkbox"><i class="fas fa-bars fa-2x" id="header__checkbox--menu"></i></label>
      <nav>
        <ul>
          <li><a class="link--primary" href="https://safe-migrants-nantes.org/qui-sommes-nous">Qui sommes-nous ?</a></li>
          <li><a class="link--primary" href="https://safe-migrants-nantes.org/#nous-soutenir">Nous soutenir</a></li>
          <li><a class="link--primary" href="https://safe-migrants-nantes.org/liste-des-articles">Actualités</a></li>
          <li><a class="link--primary" href="https://safe-migrants-nantes.org#contact">Contact</a></li>
        </ul>
      </nav>
    </header>
    <!-- Background-header-->
    <div class="header__background background--pages"></div>
    <main>
      <h2 class="title title--line"><span>PRESENTATION</span></h2>
      <p>
         Safe Migrants Nantes est une association née à la rentrée 2018 à but non lucratif, dont les propos, les actions,
        les interventions visent à promouvoir une intuition fondamentale : dans ce monde aucun être humain n’est illégal. À ce titre,
        la place des migrants en ce début de XXIème siècle nous questionne tout autant sur le monde que sur notre propre société.
      </p>
      <h2 class="title title--line"><span>NOS OBJECTIFS</span></h2>
      <p>
        L’équipe de départ, composée de migrants, d’étudiants et de médecins, s’est constituée lors de l’occupation du campus par
        des migrants et leurs soutiens. Dans l’urgence, une série d’actions a été entreprise, en relation avec d’autres collectifs.
        Le bilan d’un an d’expériences exige de pérenniser cette perspective, en complément avec le tissu associatif actif sur ces
        thématiques.
      </p>
      <p>
        Notre action se propose de donner les outils nécessaires aux migrants afin qu’ils puissent devenir maîtres de leur
        destin. Cette responsabilisation active n’est possible qu’à condition de construire un socle solide qui repose sur
        trois bases : la santé, l’éducation, et la garantie des droits. Ce sont des critères essentiels à nos yeux qui peuvent
        permettre aux migrants d’envisager un avenir inséré dans la société, et un séjour durable.
      </p>
      <h2 class="title title--line"><span>COMMISSIONS</span></h2><a id="education"></a>
      <h3 class="title--list"><span>Éducation & formation</span></h3>
      <p>
        Essentiellement issus du campus, nous avons impulsé un travail d’inscriptions de migrants à l’université de Nantes, dans
        quelques lycées généraux et professionnels. Un groupe d’enseignants se constitue en commission éducation pour assurer – en
        collaboration avec d’autres – un travail essentiel d’apprentissage du français et de remise à niveau.
      </p><a id="sante"></a>
      <h3 class="title--list"><span>Santé</span></h3>
      <p>
        Avec plus d’une quinzaine de professionnels de santé (généralistes, dentistes, psychiatres, infirmières, kinésithérapeutes,
        ostéopathes, etc.) notre commission santé a assuré et continue d’assurer des permanences santé sur le campus, dans les lieux
        occupés, et dernièrement aux Bains Douches dans une salle municipale.
      </p>
      <p>
        En un an, ce sont, ce sont plusieurs centaines de patients qui ont pu être traités hors du circuit sanitaire désormais
        saturé –enfants, adolescents et adultes, de la Guinée au Soudan, de la Tunisie à l’Irak. La possibilité pour des étudiants
        en santé de valider des stages (avec des professionnels qui assureront les actes médicaux) permettra de consolider notre
        expérience de médecine d’urgence.	
      </p><a id="communication"></a>
      <h3 class="title--list"><span>Communication</span></h3>
      <p>
        Une exposition sur le campus à la bibliothèque universitaire a	rendu visible la parole des migrants ; une réunion
        publique en association avec tout l’arc syndical a présenté au public nantais leurs parcours, leurs souffrances et leurs
        espoirs. La commission communication se donne pour but de continuer dans cette perspective afin de donner toujours plus la
        parole aux migrants.
      </p>
      <h3 class="title--list"><span>Droits</span></h3>
      <p>
        L’association se donne pour objectif également d’accompagner les migrants juridiquement. La préparation de leurs
        dossiers en amont permettrait à des experts juridiques d’autres structures de n’avoir qu’à les finaliser. Cette
        commission n’existe pas encore, c’est donc à un terrain en chantier que chacun est appelé à s’atteler, et en particulier
        des étudiants en droit, dont la pratique juridique nous serait d’un grand secours.
      </p>
      <h2 class="title title--line"><span>UNE FAÇON DE TRAVAILLER INCLUSIVE</span></h2>
      <p>
        Le propos de Safe migrants Nantes n’est pas de se substituer aux initiatives existantes, mais de coordonner des énergies
        en vue de réaliser les principes fondateurs de l’association. C’est pourquoi nous collaborons de façon fructueuse avec des
        syndicats de travailleurs, des associations, des collectifs, mais aussi des associations africaines chrétiennes et
        musulmanes.
      </p>
      <h2 class="title title--line"><span>EXTRAIT DU PRÉAMBULE DE L'ASSOCIATION</span></h2>
      <blockquote>
        <p>
          Notre action est venue compléter l’activité associative et militante auprès des migrants de la métropole nantaise, qui
          sont en nombre croissant et sont confrontés à l’insuffisance de moyens face à la demande. […] C’est pourquoi il nous est
          apparu nécessaire de formaliser nos actions à travers une association, afin de structurer et rendre plus efficaces nos
          interventions auprès des migrants de la métropole nantaise.
        </p>
        <p>
          Cette décision, prise à la suite de plusieurs contacts et rencontres avec des représentants de l’État et de Nantes
          Métropole, a également pour but d’obtenir une reconnaissance de la part des autorités et des partenaires institutionnels,
          et de pouvoir bénéficier des aides et des subventions nécessaires pour mener à bien nos activités.
        </p>
        <cite>—Extrait du préambule de l'association	</cite>
      </blockquote>
      <div class="article__donate"><span><i class="fas fa-2x fa-heart" style="color:#f12d2d"></i></span><span>Safe Migrants Nantes dépend de votre soutien pour financer les accès aux soins et à l'éducation des sans-papiers. <a class="highlight--accent link--accent" href="https://www.leetchi.com/c/pour-lacces-des-sans-papiers-a-leducation-wkp9n3q5" target="_blank" rel="noreferrer">Faites un don →</a></span></div>
    </main>
    <!-- Footer--><a id="contact"></a>
    <footer>
      <div class="social-footer__container">
        <div class="newsletter">
          <h2>Recevez la newsletter<br>de l'association !</h2>
          <form id="newsletterForm">
            <div class="form__container">
              <div class="field__container">
                <input type="email" name="email" required>
                <label class="field__label" for="email"><span>Adresse email</span></label>
              </div>
              <div class="newsletter__btn">
                <input id="newsletterButton" type="submit" value="S'ABONNER">
              </div>
            </div>
          </form>
        </div>
        <div class="contact">
          <h2>Contact</h2>
          <ul>
            <li><a class="btn-social--primary" href="mailto:safe-migrants-nantes@laposte.net" target="_blank" rel="noreferrer" aria-label="Email de Safe Migrants Nantes"><i class="fas fa-envelope fa-2x"></i></a></li>
            <li><a class="btn-social--primary" href="https://www.facebook.com/pg/CSPN44" target="_blank" rel="noreferrer" aria-label="Facebook de Safe Migrants Nantes"><i class="fab fa-facebook-square fa-2x"></i></a></li>
            <li><a class="btn-social--primary" href="https://www.messenger.com/t/CSPN44" target="_blank" rel="noreferrer" aria-label="Messenger de Safe Migrants Nantes"><i class="fab fa-facebook-messenger fa-2x"></i></a></li>
          </ul>
        </div>
      </div>
      <div class="mentions-legales">
        <h2>Mentions légales</h2>
        <div>
          <p>Un site réalisé avec ❤️ par <span class="highlight--primary">Ekkaia</span></p>
          <p>Crédit photo (page d'accueil) : <a class="link--primary highlight--primary" href="https://www.warrenrichardson.com" rel="noreferrer">Warren Richardson</a></p>
          <p>Consulter les mentions légales : <a class="link--primary highlight--primary" href="https://safe-migrants-nantes/mentions-legales">Mentions légales</a></p>
          <p><a class="link--primary highlight--primary" href="redacteur/gestion">Accès rédacteur <i class="fas fa-lock"></i></a></p>
        </div>
      </div>
    </footer>
    <script src="js/script.js"></script>
  </body>
</html>