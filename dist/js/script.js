// Header
const navHeader = document.getElementById("header");

window.addEventListener("scroll", () => {
	if (window.scrollY > 0) {
		navHeader.classList.add("header--fixed");
	} else {
		navHeader.classList.remove("header--fixed");
	}
});

const navMenu = document.getElementById("header__checkbox--menu");
const checkbox = document.getElementById("header__checkbox");

checkbox.addEventListener("change", () => {
	if (checkbox.checked) {
		navMenu.classList.add("fa-times");
		navMenu.classList.remove("fa-bars");
		navHeader.classList.add("header--deploy");
	} else {
		navMenu.classList.add("fa-bars");
		navMenu.classList.remove("fa-times");
		navHeader.classList.remove("header--deploy");
	}
});

// Redaction
function pushTextarea(balise) {
	let textarea = document.getElementById("textarea");
	let textareaValue = textarea.value;
	let startPos = textarea.selectionStart;
	let startSub = textareaValue.substring(0, startPos);
	let endSub = textareaValue.substring(textarea.selectionEnd, textareaValue.length);
	textarea.value = startSub + balise + endSub;
	textarea.focus();
	switch (balise) {
		case "<p></p>":
			textarea.setSelectionRange(startPos + balise.length - 4, startPos + balise.length - 4);
			break;
		case "<h3 class='title--list'><span></span></h3>":
			textarea.setSelectionRange(startPos + balise.length - 12, startPos + balise.length - 12);
			break;
		case "<blockquote><p></p></blockquote>":
			textarea.setSelectionRange(startPos + balise.length - 17, startPos + balise.length - 17);
			break;
		case "<a href='[Adresse du lien]' class='link-article--accent'></a>":
			textarea.setSelectionRange(startPos + balise.length - 4, startPos + balise.length - 4);
			break;
		case "<i></i>":
			textarea.setSelectionRange(startPos + balise.length - 4, startPos + balise.length - 4);
			break;
	}
}

function fileInfo() {
	let fileUploader = document.getElementById("file");
	let label = document.getElementById("label");
	let descImage = document.getElementById("descImage");

	fileUploader.addEventListener("change", () => {
		label.innerHTML = `Vous avez sélectionné : <span style="color:#f12d2d">${fileUploader.files[0].name}</span><br /><span style="font-size:0.75em">(Taille : ${fileConvertSize(fileUploader.files[0].size)})</span>`;
		let img = document.createElement("img");
		img.src = URL.createObjectURL(event.target.files[0]);
		img.setAttribute("width", "25%");
		img.setAttribute("height", "25%");
		label.append(img);
		descImage.style.display = "block";
	});
}

// Modal
function modal(state) {
	let modal = document.getElementsByTagName("dialog")[0];
	if (state === "close") {
		modal.style.display = "none";
	} else {
		modal.style.display = "block";
		let deleteLink = document.getElementById("deleteLink");
		deleteLink.href = `../php/main?delete=${state}`;
	}
}

function fileConvertSize(size){
	size = Math.abs(parseInt(size, 10));
	let def = [[1, "octets"], [1024, "ko"], [1024*1024, "Mo"], [1024*1024*1024, "Go"], [1024*1024*1024*1024, "To"]];
	for(let i = 0 ; i < def.length ; i++) {
		if (size < def[i][0]) {
			return `${(size / def[i - 1][0]).toFixed(2)} ${def[i-1][1]}`;
		}
	}
}

// Newsletter
const newsletterForm = document.getElementById("newsletterForm");
const newsletterButton = document.getElementById("newsletterButton");

newsletterForm.addEventListener("submit", (event) => {
	event.preventDefault();

	let formData = new FormData(newsletterForm);

	fetch("php/main.php", {
		method: "POST",
		mode: "same-origin",
		credentials: "same-origin",
		body: formData
	}).then(function (response) {
		return response.text();
	}).then(function (data) {
		newsletterButton.value = data;
		newsletterButton.disabled = true;
	}).catch(function (error) {
		alert(error);
	})

	newsletterForm.reset();
});

// Redaction
const redactionForm = document.getElementById("redactionForm");
const redactionButton = document.getElementById("redactionButton");

redactionForm.addEventListener("submit", (event) => {
	event.preventDefault();

	let formData = new FormData(redactionForm);

	let params = new URLSearchParams(document.location.search.substring(1));
	let slug = params.get("slug");

	if (slug != null) {
		formData.append("slug", slug);
	}

	fetch("../php/main.php", {
		method: "POST",
		mode: "same-origin",
		credentials: "same-origin",
		body: formData
	}).then(function (response) {
		return response.text();
	}).then((data) => {
		if (data === "Veuillez remplir tous les champs !" || data === "La requête a échouée !" || data === "Votre image doit être au format jpg !" || data === "Vous n'avez pas légendé votre image !") {
			let alert = document.getElementsByClassName("alert")[0];
			alert.innerHTML = `<div class='alert--error'><p><i class='fas fa-exclamation-triangle'></i> Une erreur est survenue : <span class='highlight--dark'>${data}</span>.</p></div>`;
			document.body.scrollTop = document.documentElement.scrollTop = 0;
		} else {
			window.location = `../articles/${data}`;
		}
	}).catch((error) => {
		alert(error);
	});
});