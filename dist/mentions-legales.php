<!DOCTYPE html>
<!--
███████╗ █████╗ ███████╗███████╗    ███╗   ███╗██╗ ██████╗ ██████╗  █████╗ ███╗   ██╗████████╗███████╗
██╔════╝██╔══██╗██╔════╝██╔════╝    ████╗ ████║██║██╔════╝ ██╔══██╗██╔══██╗████╗  ██║╚══██╔══╝██╔════╝
███████╗███████║█████╗  █████╗█████╗██╔████╔██║██║██║  ███╗██████╔╝███████║██╔██╗ ██║   ██║   ███████╗
╚════██║██╔══██║██╔══╝  ██╔══╝╚════╝██║╚██╔╝██║██║██║   ██║██╔══██╗██╔══██║██║╚██╗██║   ██║   ╚════██║
███████║██║  ██║██║     ███████╗    ██║ ╚═╝ ██║██║╚██████╔╝██║  ██║██║  ██║██║ ╚████║   ██║   ███████║
╚══════╝╚═╝  ╚═╝╚═╝     ╚══════╝    ╚═╝     ╚═╝╚═╝ ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═══╝   ╚═╝   ╚══════╝
-->
<html lang="fr" prefix="og: http://ogp.me/ns#">
  <head>
    <meta property="og:type" content="website">
    <title>Mentions légales — Safe Migrants Nantes</title>
    <link rel="stylesheet" href="stylesheet/style.css">
    <link rel="icon" type="image/png" href="./images/favicon.png">
    <meta charset="UTF-8">
    <meta name="theme-color" content="#f79f24">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- OGP-->
    <meta name="description" content="Mentions légales de l'association SAFE Migrants Nantes.">
    <meta property="og:site_name" content="Safe Migrants Nantes"><?php $url = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" ?>
    <meta property="og:url" content="<?= $url ?>">
    <meta property="og:title" content="Mentions légales">
    <meta property="og:description" content="Mentions légales de l'association SAFE Migrants Nantes.">
    <meta property="og:image" content="https://safe-migrants-nantes.org/images/logo.jpg">
    <meta property="og:locale" content="fr_FR">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
  </head>
  <body>
    <!-- Navigation-->
    <header id="header">
      <div><a class="header__link" href="https://safe-migrants-nantes.org"><img class="header__logo" src="https://safe-migrants-nantes.org/images/logo.svg" alt="Logo de Safe Migrants Nantes">
          <p class="header__title highlight--primary">SAFE MIGRANTS NANTES</p></a></div>
      <input id="header__checkbox" type="checkbox">
      <label id="header__checkbox--label" for="header__checkbox"><i class="fas fa-bars fa-2x" id="header__checkbox--menu"></i></label>
      <nav>
        <ul>
          <li><a class="link--primary" href="https://safe-migrants-nantes.org/qui-sommes-nous">Qui sommes-nous ?</a></li>
          <li><a class="link--primary" href="https://safe-migrants-nantes.org/#nous-soutenir">Nous soutenir</a></li>
          <li><a class="link--primary" href="https://safe-migrants-nantes.org/liste-des-articles">Actualités</a></li>
          <li><a class="link--primary" href="https://safe-migrants-nantes.org#contact">Contact</a></li>
        </ul>
      </nav>
    </header>
    <!-- Background-header-->
    <div class="header__background background--pages"></div>
    <main class="mentions-legales">
      <h2 class="title title--line"><span>ÉDITION DU SITE</span></h2>
      <p>
         Ce site est édité par <span class="highlight--dark">SAFE Migrants Nantes </span>(association loi 1901).</p>
      <ul>
        <li> 
          <p><span class="highlight--dark">Adresse :</span></p>
        </li>
        <li>
          <p><span class="highlight--dark">Contact mail : </span><a class="link--accent highlight--accent" href="mailto:safe-migrants-nantes@laposte.net" target="_blank" rel="noreferrer">safe-migrants-nantes@laposte.net</a></p>
        </li>
        <li> 
          <p><span class="highlight--dark">Représenté par :</span></p>
        </li>
      </ul>
      <h2 class="title title--line"><span>HÉBERGEMENT DU SITE</span></h2>
      <p>Ce site est hebergé par <a class="link--accent highlight--accent" href="https://www.ikoula.com/fr" target="_blank" rel="noreferrer">Ikoula</a>.</p>
      <ul>
        <li> 
          <p><span class="highlight--dark">Siège social : </span>175/177 rue d'Aguesseau, 92100 Boulogne Billancourt, FRANCE</p>
        </li>
        <li>
          <p><span class="highlight--dark">Contact : </span><a class="link--accent highlight--accent" href="mailto:support@ikoula.com" target="_blank" rel="noreferrer">support@ikoula.com</a></p>
        </li>
      </ul>
      <h2 class="title title--line"><span>RÉALISATION DU SITE</span></h2>
      <p>Ce site à été conçu et réalisé par <span class="highlight--dark">Ekkaia</span>.</p>
      <h2 class="title title--line"><span>UTILISATION DU SITE</span></h2>
      <h3 class="title--list"><span>Accès au site</span></h3>
      <p>Ce site est accessible gratuitement à tout utilisateur ayant accès à internet.</p>
      <p>
        Tous les frais inhérents à l'accès au site internet sont à la charge de l'utilisateur 
        (la connexion internet, le matériel, les logiciels, ect) qui est le seul responsable du 
        bon fonctionnement de son équipement informatique et de l'accès à une connexion internet.
      </p>
      <p>L'association <span class="highlight--dark">SAFE Migrants Nantes </span>ne peut être tenue pour responsable de tout dysfonctionnement 
        liés à l'accès à son site internet. Des problèmes liés aux serveurs ou au réseau dégradant ou empêchant cet 
        accès ne sauraient engager sa responsabilité.
      </p>
      <p>
        L'association étant seule gérante de ce site internet, elle se réserve le droit d'interrompre son accès 
        pour cause de maintenance ou toute autre raison. Aucunes obligations ni indemnisations ne 
        peuvent découler de cette interruption momentanée, et sans préavis.
      </p>
      <h3 class="title--list"><span>Propriété intellectuelle</span></h3>
      <p>Le contenu du site internet de <span class="highlight--dark">SAFE Migrants Nantes </span>est protégé par le droit en vigueur régissant la propriété intellectuelle.</p>
      <p>
        Le contenu produit ou reproduit sur le site internet est l'objet d'un droit d'auteur. Toute reproduction ou diffusion 
        sans autorisations préalables expresses écrite de <span class="highlight--dark">SAFE Migrants Nantes </span>constitue une infraction passible de sanctions pénales.
      </p>
      <h3 class="title--list"><span>Données personnelles</span></h3>
      <p>
        Au visa du droit au respect de la vie privée de ses utilisateurs, lesquels peuvent être amené à communiquer des données 
        personnelles au sein du présent site, <span class="highlight--dark">SAFE Migrants Nantes </span>s'engage à ce que la collecte et le traitement de ces données soient réalisés conformément à la loi n°78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés modifiée par la loi 2004-801 du 6 aout 2004. 
      </p>
      <p>La loi énoncée garantit à l'utilisateur un droit d'opposition, d'accès et de rectification sur les données nominatives le concernant. </p>
      <p>Ce droit sera exercé aussitôt que la demande en sera faite auprès de <span class="highlight--dark">SAFE Migrants Nantes </span>à l'adresse suivante : <a class="link--accent highlight--accent" href="mailto:safe-migrants-nantes@laposte.net" target="_blank" rel="noreferrer">safe-migrants-nantes@laposte.net</a></p>
      <h3 class="title--list"><span>Limite de responsabilité</span></h3>
      <p>L'ensemble des informations présentes sur le site de <span class="highlight--dark">SAFE Migrants Nantes </span>proviennent de sources réputées fiables. Cependant <span class="highlight--dark">SAFE Migrants Nantes </span>ne peut garantir l'exactitude et la pertinence de ces données, et ce de façon permanente.</p>
      <p>Ces données sont présentes sur le site internet à titre informatif et ne constituent en aucuns cas un conseil ou une recommandation de quelque nature que ce soit à l'égard de l'utilisateur. Par conséquent, l'utilisation de ces données ne sauraient en aucun cas engager la responsabilité de <span class="highlight--dark">SAFE Migrants Nantes </span>à quelque titre que ce soit. L'utilisateur avec discernement, esprit et raison fait usage de ces données mises à sa disposition sur le site internet.</p>
      <p>L'utilisateur s'engage à respecter l'ensemble des textes de loi en vigueur en France.</p>
      <h3 class="title--list"><span>Liens hypertexte</span></h3>
      <p>Les données et le contenu des liens hypertextes présents sur le site internet de <span class="highlight--dark">SAFE Migrants Nantes </span>ne sauraient en aucuns cas engager sa responsabilité à quelque titre que ce soit.</p>
      <h2 class="title title--line"><span>DURÉE</span></h2>
      <p>Le présent contrat est conclu pour une durée indéterminée à compter de l’utilisation du site internet de <span class="highlight--dark">SAFE Migrants </span>Nantes par l'utilisateur.</p>
      <div class="article__donate"><span><i class="fas fa-2x fa-heart" style="color:#f12d2d"></i></span><span>Safe Migrants Nantes dépend de votre soutien pour financer les accès aux soins et à l'éducation des sans-papiers. <a class="highlight--accent link--accent" href="https://www.leetchi.com/c/pour-lacces-des-sans-papiers-a-leducation-wkp9n3q5" target="_blank" rel="noreferrer">Faites un don →</a></span></div>
    </main>
    <!-- Footer--><a id="contact"></a>
    <footer>
      <div class="social-footer__container">
        <div class="newsletter">
          <h2>Recevez la newsletter<br>de l'association !</h2>
          <form id="newsletterForm">
            <div class="form__container">
              <div class="field__container">
                <input type="email" name="email" required>
                <label class="field__label" for="email"><span>Adresse email</span></label>
              </div>
              <div class="newsletter__btn">
                <input id="newsletterButton" type="submit" value="S'ABONNER">
              </div>
            </div>
          </form>
        </div>
        <div class="contact">
          <h2>Contact</h2>
          <ul>
            <li><a class="btn-social--primary" href="mailto:safe-migrants-nantes@laposte.net" target="_blank" rel="noreferrer" aria-label="Email de Safe Migrants Nantes"><i class="fas fa-envelope fa-2x"></i></a></li>
            <li><a class="btn-social--primary" href="https://www.facebook.com/pg/CSPN44" target="_blank" rel="noreferrer" aria-label="Facebook de Safe Migrants Nantes"><i class="fab fa-facebook-square fa-2x"></i></a></li>
            <li><a class="btn-social--primary" href="https://www.messenger.com/t/CSPN44" target="_blank" rel="noreferrer" aria-label="Messenger de Safe Migrants Nantes"><i class="fab fa-facebook-messenger fa-2x"></i></a></li>
          </ul>
        </div>
      </div>
      <div class="mentions-legales">
        <h2>Mentions légales</h2>
        <div>
          <p>Un site réalisé avec ❤️ par <span class="highlight--primary">Ekkaia</span></p>
          <p>Crédit photo (page d'accueil) : <a class="link--primary highlight--primary" href="https://www.warrenrichardson.com" rel="noreferrer">Warren Richardson</a></p>
          <p>Consulter les mentions légales : <a class="link--primary highlight--primary" href="https://safe-migrants-nantes/mentions-legales">Mentions légales</a></p>
          <p><a class="link--primary highlight--primary" href="redacteur/gestion">Accès rédacteur <i class="fas fa-lock"></i></a></p>
        </div>
      </div>
    </footer>
    <script src="js/script.js"></script>
  </body>
</html>