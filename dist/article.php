<!DOCTYPE html>
<!--
███████╗ █████╗ ███████╗███████╗    ███╗   ███╗██╗ ██████╗ ██████╗  █████╗ ███╗   ██╗████████╗███████╗
██╔════╝██╔══██╗██╔════╝██╔════╝    ████╗ ████║██║██╔════╝ ██╔══██╗██╔══██╗████╗  ██║╚══██╔══╝██╔════╝
███████╗███████║█████╗  █████╗█████╗██╔████╔██║██║██║  ███╗██████╔╝███████║██╔██╗ ██║   ██║   ███████╗
╚════██║██╔══██║██╔══╝  ██╔══╝╚════╝██║╚██╔╝██║██║██║   ██║██╔══██╗██╔══██║██║╚██╗██║   ██║   ╚════██║
███████║██║  ██║██║     ███████╗    ██║ ╚═╝ ██║██║╚██████╔╝██║  ██║██║  ██║██║ ╚████║   ██║   ███████║
╚══════╝╚═╝  ╚═╝╚═╝     ╚══════╝    ╚═╝     ╚═╝╚═╝ ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═══╝   ╚═╝   ╚══════╝
-->
<html lang="fr" prefix="og: http://ogp.me/ns#">
  <head><?php include "php/main.php" ?>
    <meta property="og:article:published_time" content="<?= $date_time_publication ?>">
    <meta property="og:article:modified_time" content="<?= $date_time_edition ?>">
    <meta property="og:type" content="article">
    <meta property="og:article:author" content="<?= $auteur ?>">
    <title><?= $titre ?> — Safe Migrants Nantes</title>
    <link rel="stylesheet" href="../stylesheet/style.css">
    <link rel="icon" type="image/png" href="../images/favicon.png">
    <meta charset="UTF-8">
    <meta name="theme-color" content="#f79f24">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- OGP-->
    <meta name="description" content="<?= $chapo ?>">
    <meta property="og:site_name" content="Safe Migrants Nantes"><?php $url = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" ?>
    <meta property="og:url" content="<?= $url ?>">
    <meta property="og:title" content="<?= $titre ?>">
    <meta property="og:description" content="<?= $chapo ?>">
    <meta property="og:image" content="https://safe-migrants-nantes.org/images/miniatures/<?= $slug ?>.jpg">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="630">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="<?= $url ?>">
    <meta name="twitter:title" content="<?= $titre ?>">
    <meta name="twitter:description”" content="<?= $chapo ?>">
    <meta name="twitter:image”" content="https://safe-migrants-nantes.org/images/miniatures/<?= $slug ?>.jpg">
    <meta property="og:locale" content="fr_FR">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
  </head>
  <body>
    <!-- Navigation-->
    <header id="header">
      <div><a class="header__link" href="https://safe-migrants-nantes.org"><img class="header__logo" src="https://safe-migrants-nantes.org/images/logo.svg" alt="Logo de Safe Migrants Nantes">
          <p class="header__title highlight--primary">SAFE MIGRANTS NANTES</p></a></div>
      <input id="header__checkbox" type="checkbox">
      <label id="header__checkbox--label" for="header__checkbox"><i class="fas fa-bars fa-2x" id="header__checkbox--menu"></i></label>
      <nav>
        <ul>
          <li><a class="link--primary" href="https://safe-migrants-nantes.org/qui-sommes-nous">Qui sommes-nous ?</a></li>
          <li><a class="link--primary" href="https://safe-migrants-nantes.org/#nous-soutenir">Nous soutenir</a></li>
          <li><a class="link--primary" href="https://safe-migrants-nantes.org/liste-des-articles">Actualités</a></li>
          <li><a class="link--primary" href="https://safe-migrants-nantes.org#contact">Contact</a></li>
        </ul>
      </nav>
    </header>
    <!-- Background-header--><?php
    $image = "images/miniatures/" . $slug . ".jpg";
    if (file_exists($image)) {
    	echo "<div class='header__background background--article' style='background-image: url(../$image)'><div class='background__legend'><p>$description_image</p></div></div>";
    } else {
    	echo "<div class='header__background background--pages'></div>";
    } ?>
    <main>
      <div class="article__header">
        <h1 class="article__title"><span><?= $titre ?></span></h1>
        <p class="article__desc"><?= $chapo ?></p>
        <ul class="article__info">
          <li class="article__author"><i class="fas fa-feather-alt"></i><?= $auteur ?></li>
          <li class="article__date"><i class="far fa-clock"></i><?= date("d/m/Y", strtotime($date_time_publication)); ?></li>
          <li class="article__tag"><i class="fas fa-tag"></i><?= $tag ?></li>
        </ul>
      </div>
      <article><?= $contenu ?></article>
      <div class="article__donate"><span><i class="fas fa-2x fa-heart" style="color:#f12d2d"></i></span><span>Safe Migrants Nantes dépend de votre soutien pour financer les accès aux soins et à l'éducation des sans-papiers. <a class="highlight--accent link--accent" href="https://www.leetchi.com/c/pour-lacces-des-sans-papiers-a-leducation-wkp9n3q5" target="_blank" rel="noreferrer">Faites un don →</a></span></div>
    </main>
    <!-- Footer--><a id="contact"></a>
    <footer>
      <div class="social-footer__container">
        <div class="newsletter">
          <h2>Recevez la newsletter<br>de l'association !</h2>
          <form id="newsletterForm">
            <div class="form__container">
              <div class="field__container">
                <input type="email" name="email" required>
                <label class="field__label" for="email"><span>Adresse email</span></label>
              </div>
              <div class="newsletter__btn">
                <input id="newsletterButton" type="submit" value="S'ABONNER">
              </div>
            </div>
          </form>
        </div>
        <div class="contact">
          <h2>Contact</h2>
          <ul>
            <li><a class="btn-social--primary" href="mailto:safe-migrants-nantes@laposte.net" target="_blank" rel="noreferrer" aria-label="Email de Safe Migrants Nantes"><i class="fas fa-envelope fa-2x"></i></a></li>
            <li><a class="btn-social--primary" href="https://www.facebook.com/pg/CSPN44" target="_blank" rel="noreferrer" aria-label="Facebook de Safe Migrants Nantes"><i class="fab fa-facebook-square fa-2x"></i></a></li>
            <li><a class="btn-social--primary" href="https://www.messenger.com/t/CSPN44" target="_blank" rel="noreferrer" aria-label="Messenger de Safe Migrants Nantes"><i class="fab fa-facebook-messenger fa-2x"></i></a></li>
          </ul>
        </div>
      </div>
      <div class="mentions-legales">
        <h2>Mentions légales</h2>
        <div>
          <p>Un site réalisé avec ❤️ par <span class="highlight--primary">Ekkaia</span></p>
          <p>Crédit photo (page d'accueil) : <a class="link--primary highlight--primary" href="https://www.warrenrichardson.com" rel="noreferrer">Warren Richardson</a></p>
          <p>Consulter les mentions légales : <a class="link--primary highlight--primary" href="https://safe-migrants-nantes/mentions-legales">Mentions légales</a></p>
          <p><a class="link--primary highlight--primary" href="../redacteur/gestion">Accès rédacteur <i class="fas fa-lock"></i></a></p>
        </div>
      </div>
    </footer>
    <script src="../js/script.js"></script>
  </body>
</html>