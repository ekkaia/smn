<!DOCTYPE html>
<!--
███████╗ █████╗ ███████╗███████╗    ███╗   ███╗██╗ ██████╗ ██████╗  █████╗ ███╗   ██╗████████╗███████╗
██╔════╝██╔══██╗██╔════╝██╔════╝    ████╗ ████║██║██╔════╝ ██╔══██╗██╔══██╗████╗  ██║╚══██╔══╝██╔════╝
███████╗███████║█████╗  █████╗█████╗██╔████╔██║██║██║  ███╗██████╔╝███████║██╔██╗ ██║   ██║   ███████╗
╚════██║██╔══██║██╔══╝  ██╔══╝╚════╝██║╚██╔╝██║██║██║   ██║██╔══██╗██╔══██║██║╚██╗██║   ██║   ╚════██║
███████║██║  ██║██║     ███████╗    ██║ ╚═╝ ██║██║╚██████╔╝██║  ██║██║  ██║██║ ╚████║   ██║   ███████║
╚══════╝╚═╝  ╚═╝╚═╝     ╚══════╝    ╚═╝     ╚═╝╚═╝ ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═══╝   ╚═╝   ╚══════╝
-->
<html lang="fr" prefix="og: http://ogp.me/ns#">
  <head>
    <meta property="og:type" content="website">
    <title>Personne n'est illégal — Safe Migrants Nantes</title>
    <link rel="stylesheet" href="stylesheet/style.css">
    <link rel="icon" type="image/png" href="./images/favicon.png">
    <meta charset="UTF-8">
    <meta name="theme-color" content="#f79f24">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- OGP-->
    <meta name="description" content="Safe Migrants Nantes est une association nantaise qui vient en aide aux sans-papiers. Nous croyons fermement que personne n'est illégal, c'est pourquoi nous agissons pour que tout le monde ait accès aux soins et à l'éducation.">
    <meta property="og:site_name" content="Safe Migrants Nantes"><?php $url = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" ?>
    <meta property="og:url" content="<?= $url ?>">
    <meta property="og:title" content="Personne n'est illégal">
    <meta property="og:description" content="Safe Migrants Nantes est une association nantaise qui vient en aide aux sans-papiers. Nous croyons fermement que personne n'est illégal, c'est pourquoi nous agissons pour que tout le monde ait accès aux soins et à l'éducation.">
    <meta property="og:image" content="https://safe-migrants-nantes.org/images/logo.jpg">
    <meta property="og:locale" content="fr_FR">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
  </head>
  <body>
    <!-- Navigation-->
    <header id="header">
      <div><a class="header__link" href="https://safe-migrants-nantes.org"><img class="header__logo" src="https://safe-migrants-nantes.org/images/logo.svg" alt="Logo de Safe Migrants Nantes">
          <p class="header__title highlight--primary">SAFE MIGRANTS NANTES</p></a></div>
      <input id="header__checkbox" type="checkbox">
      <label id="header__checkbox--label" for="header__checkbox"><i class="fas fa-bars fa-2x" id="header__checkbox--menu"></i></label>
      <nav>
        <ul>
          <li><a class="link--primary" href="https://safe-migrants-nantes.org/qui-sommes-nous">Qui sommes-nous ?</a></li>
          <li><a class="link--primary" href="https://safe-migrants-nantes.org/#nous-soutenir">Nous soutenir</a></li>
          <li><a class="link--primary" href="https://safe-migrants-nantes.org/liste-des-articles">Actualités</a></li>
          <li><a class="link--primary" href="https://safe-migrants-nantes.org#contact">Contact</a></li>
        </ul>
      </nav>
    </header>
    <!-- Header background-->
    <div class="header__background background--index">
      <div class="title__container">
        <h1 class="background__title"><span>PERSONNE N'EST</span></h1>
        <h1 class="background__title background__title--sub"><span>ILLÉGAL</span></h1>
      </div>
    </div>
    <!--Snackbar-->
    <?php
    	require("php/bdd.php");
    	$last_article = $bdd->query("SELECT titre, slug, date_time_publication FROM articles WHERE date_time_publication IN (SELECT MAX(date_time_publication) FROM articles)");
    	$last_article = $last_article->fetch();
    ?><a class="snackbar" href="./articles/<?= $last_article['slug'] ?>">
      <div class="snackbar__article">
        <p>Dernier article : <span class="snackbar__date"><?= date("d/m/Y", strtotime($last_article["date_time_publication"])); ?></span></p>
        <h3><?= $last_article["titre"] ?></h3>
      </div>
      <div class="snackbar__icon"><i class="fas fa-arrow-right"></i></div></a>
    <!-- Qui sommes nous ?--><a id="qui-sommes-nous"></a>
    <section>
      <h2 class="title title--line"><span>QUI SOMMES-NOUS ?</span></h2>
      <div class="presentation__container">
        <div class="presentation__content">
          <p>
            Safe Migrants Nantes est une association loi 1901
            née à la rentrée 2018 dans la continuité du Collectif des sans-papiers de Nantes.
            Cette association à but non lucratif vise à promouvoir une intuition fondamentale :
            dans ce monde aucun être humain n’est illégal.
          </p>
          <p>
            Notre travail est réparti en commissions, où chacun est appelé à s’investir à la hauteur de ses compétences et
            de ses envies.	
          </p>
        </div>
        <div class="logo__container"><img class="logo__img" src="images/logo.jpg" alt="Logo de Safe-Migrants-Nantes"></div>
      </div>
      <div class="commission__container">
        <div class="commission__left">
          <h3><a class="highlight--accent link--accent" href="qui-sommes-nous#sante">Santé</a></h3>
          <p>
            Une commission santé, qui organise des permanences santé assurées par des professionnels de santé :
            médecins, psychiatres, infirmières, ostéopathes.
          </p>
        </div>
        <div class="commission__central">
          <h3><a class="highlight--accent link--accent" href="qui-sommes-nous#education">Éducation</a></h3>
          <p>
            Une commission éducation, dont la mission est d’accompagner administrativement et pédagogiquement les sans-papiers
            dans leur inscription à l’université et dans les lycées à la hauteur de nos forces et de nos moyens.
          </p>
        </div>
        <div class="commission__right">
          <h3><a class="highlight--accent link--accent" href="qui-sommes-nous#communication">Communication</a></h3>
          <p>
            Une commission communication, afin de rendre audibles et visibles les migrants, leur parcours,
            les obstacles rencontrés en France, et d’assurer l’apparition publique de l’association.
          </p>
        </div>
      </div>
      <div class="btn__container"><a class="btn btn--accent" href="qui-sommes-nous">DÉCOUVRIR L'ASSOCIATION</a></div>
    </section>
    <!-- Nous soutenir--><a id="nous-soutenir"></a>
    <section id="nous-soutenir-container">
      <h2 class="title title--line"><span>NOUS SOUTENIR</span></h2>
      <div class="support__container">
        <p>Les frais d'inscription dans un lycée peuvent s'élever <span class="highlight--dark">jusqu'à 3 000€</span>. Cette contrainte financière nous empêchant d’avancer dans les démarches d'inscription des jeunes
          	sans-papiers, nous faisons appel à vous et à votre <span class="highlight--dark">générosité</span>. L’accès à l‘éducation est un <span class="highlight--dark">droit fondamental</span>, soyons solidaires pour permettre l’intégration de ces nouveaux nantais.
        </p>
        <div class="btn__container"><a class="btn btn--dark" href="https://www.leetchi.com/c/pour-lacces-des-sans-papiers-a-leducation-wkp9n3q5" target="_blank" rel="noreferrer">FAIRE UN DON</a></div>
      </div>
    </section>
    <!-- Articles--><a id="actualites"></a>
    <section>
      <h2 class="title title--line"><span>ACTUALITÉS</span></h2>
      <div class="article-card__container">
        <?php
        	require("php/bdd.php");
        	$articles = $bdd->query("SELECT * FROM articles ORDER BY date_time_publication DESC LIMIT 3");
        	while ($a = $articles->fetch()) {
        ?><a class="article-card" href="articles/<?= $a['slug'] ?>">
          <div class="article-card__img-container">
            <?php
            $slug = $a["slug"];
            $image = "images/miniatures/$slug.jpg";
            $description_image = $a["description_image"];
            if (file_exists($image)) {
            	echo "<img class='article-card__img' alt='$description_image' src='$image' />";
            } else {
            	echo "<img class='article-card__img' alt='« Hope For A New Life », Warren Richardson' src='images/headerBackground.jpg' />";
            } ?>
          </div>
          <div class="article-card__content">
            <h3 class="article-card__title"><?= $a["titre"] ?></h3>
            <p class="article-card__desc"><?= $a["chapo"] ?></p>
            <div class="article__info"><span class="article__author"><i class="fas fa-feather-alt"></i><?= $a["auteur"] ?></span><span class="article__date"><i class="far fa-clock"></i><?= date("d/m/Y", strtotime($a["date_time_publication"])); ?></span></div>
          </div></a><?php } ?>
      </div>
      <div class="btn__container"><a class="btn btn--accent" href="liste-des-articles">VOIR TOUTES LES ACTUALITÉS</a></div>
    </section>
    <!-- Footer--><a id="contact"></a>
    <footer>
      <div class="social-footer__container">
        <div class="newsletter">
          <h2>Recevez la newsletter<br>de l'association !</h2>
          <form id="newsletterForm">
            <div class="form__container">
              <div class="field__container">
                <input type="email" name="email" required>
                <label class="field__label" for="email"><span>Adresse email</span></label>
              </div>
              <div class="newsletter__btn">
                <input id="newsletterButton" type="submit" value="S'ABONNER">
              </div>
            </div>
          </form>
        </div>
        <div class="contact">
          <h2>Contact</h2>
          <ul>
            <li><a class="btn-social--primary" href="mailto:safe-migrants-nantes@laposte.net" target="_blank" rel="noreferrer" aria-label="Email de Safe Migrants Nantes"><i class="fas fa-envelope fa-2x"></i></a></li>
            <li><a class="btn-social--primary" href="https://www.facebook.com/pg/CSPN44" target="_blank" rel="noreferrer" aria-label="Facebook de Safe Migrants Nantes"><i class="fab fa-facebook-square fa-2x"></i></a></li>
            <li><a class="btn-social--primary" href="https://www.messenger.com/t/CSPN44" target="_blank" rel="noreferrer" aria-label="Messenger de Safe Migrants Nantes"><i class="fab fa-facebook-messenger fa-2x"></i></a></li>
          </ul>
        </div>
      </div>
      <div class="mentions-legales">
        <h2>Mentions légales</h2>
        <div>
          <p>Un site réalisé avec ❤️ par <span class="highlight--primary">Ekkaia</span></p>
          <p>Crédit photo (page d'accueil) : <a class="link--primary highlight--primary" href="https://www.warrenrichardson.com" rel="noreferrer">Warren Richardson</a></p>
          <p>Consulter les mentions légales : <a class="link--primary highlight--primary" href="https://safe-migrants-nantes/mentions-legales">Mentions légales</a></p>
          <p><a class="link--primary highlight--primary" href="redacteur/gestion">Accès rédacteur <i class="fas fa-lock"></i></a></p>
        </div>
      </div>
    </footer>
    <script src="js/script.js"></script>
  </body>
</html>