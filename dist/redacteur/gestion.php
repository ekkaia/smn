<!DOCTYPE html>
<!--
███████╗ █████╗ ███████╗███████╗    ███╗   ███╗██╗ ██████╗ ██████╗  █████╗ ███╗   ██╗████████╗███████╗
██╔════╝██╔══██╗██╔════╝██╔════╝    ████╗ ████║██║██╔════╝ ██╔══██╗██╔══██╗████╗  ██║╚══██╔══╝██╔════╝
███████╗███████║█████╗  █████╗█████╗██╔████╔██║██║██║  ███╗██████╔╝███████║██╔██╗ ██║   ██║   ███████╗
╚════██║██╔══██║██╔══╝  ██╔══╝╚════╝██║╚██╔╝██║██║██║   ██║██╔══██╗██╔══██║██║╚██╗██║   ██║   ╚════██║
███████║██║  ██║██║     ███████╗    ██║ ╚═╝ ██║██║╚██████╔╝██║  ██║██║  ██║██║ ╚████║   ██║   ███████║
╚══════╝╚═╝  ╚═╝╚═╝     ╚══════╝    ╚═╝     ╚═╝╚═╝ ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═══╝   ╚═╝   ╚══════╝
-->
<html lang="fr" prefix="og: http://ogp.me/ns#">
  <head><?php include "../php/main.php" ?>
    <meta property="og:type" content="website">
    <title>Rédaction — Safe Migrants Nantes</title>
    <link rel="stylesheet" href="../stylesheet/style.css">
    <link rel="icon" type="image/png" href="../images/favicon.png">
    <meta charset="UTF-8">
    <meta name="theme-color" content="#f79f24">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- OGP-->
    <meta name="description" content="Page de rédaction des articles de Safe Migrants Nantes.">
    <meta property="og:site_name" content="Safe Migrants Nantes"><?php $url = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" ?>
    <meta property="og:url" content="<?= $url ?>">
    <meta property="og:title" content="Rédaction">
    <meta property="og:description" content="Page de rédaction des articles de Safe Migrants Nantes.">
    <meta property="og:image" content="https://safe-migrants-nantes.org/images/logo.jpg">
    <meta property="og:locale" content="fr_FR">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
  </head>
  <body>
    <!-- Navigation-->
    <header id="header">
      <div><a class="header__link" href="https://safe-migrants-nantes.org"><img class="header__logo" src="https://safe-migrants-nantes.org/images/logo.svg" alt="Logo de Safe Migrants Nantes">
          <p class="header__title highlight--primary">SAFE MIGRANTS NANTES</p></a></div>
      <input id="header__checkbox" type="checkbox">
      <label id="header__checkbox--label" for="header__checkbox"><i class="fas fa-bars fa-2x" id="header__checkbox--menu"></i></label>
      <nav>
        <ul>
          <li><a class="link--primary" href="https://safe-migrants-nantes.org/qui-sommes-nous">Qui sommes-nous ?</a></li>
          <li><a class="link--primary" href="https://safe-migrants-nantes.org/#nous-soutenir">Nous soutenir</a></li>
          <li><a class="link--primary" href="https://safe-migrants-nantes.org/liste-des-articles">Actualités</a></li>
          <li><a class="link--primary" href="https://safe-migrants-nantes.org#contact">Contact</a></li>
        </ul>
      </nav>
    </header>
    <!-- Background-header-->
    <div class="header__background background--pages"></div>
    <section class="gestion">
      <div class="alert">
        <?php 
        	if (isset($_GET["success"]) AND !empty($_GET["success"])) {
        		$success = $_GET["success"];
        		echo displaySuccessMessage($success);
        	} else if (isset($_GET["error"]) AND !empty($_GET["error"])) {
        		$error = $_GET["error"];
        		echo displayErrorMessage($error);	
        	}
        ?>
      </div>
      <h2 class="title"><span>LISTE DES ARTICLES EN LIGNE</span></h2>
      <ul class="gestion__list">
        <?php
        	$articles = $bdd->query("SELECT * FROM articles ORDER BY date_time_publication DESC");
        	while ($a = $articles->fetch()) {
        ?>
        <li class="list__article">
          <div><a class="link-article--primary" href="../articles/<?= $a['slug'] ?>"><?= $a["titre"] ?> </a><span class="list__article-info">
              <?= $a["auteur"] ?> — <?= date("d/m/Y", strtotime($a["date_time_publication"])); ?>
              <?php if ($a["date_time_edition"] != NULL) { ?>
              	(modifié le : <?= date("d/m/Y", strtotime($a["date_time_edition"])); ?> à <?= date("G:i", strtotime($a["date_time_edition"])); ?>)
              <?php } ?></span></div>
          <div class="list__btn"><a class="btn btn--accent" href="redaction.php?slug=<?= $a['slug'] ?>"> <i class="fas fa-edit"></i><span>ÉDITER</span></a><a class="btn btn--accent" onclick="modal('<?= $a['slug'] ?>')"> <i class="fas fa-trash"></i><span>SUPPRIMER</span></a></div>
        </li><?php } ?>
      </ul>
      <div><a class="btn btn--primary" href="./redaction">CRÉER UN NOUVEL ARTICLE</a></div>
      <h2 class="title"><span>LISTE DES ABONNÉS À LA NEWSLETTER</span></h2>
      <ul class="gestion__list">
        <?php
        	$newsletter = $bdd->query("SELECT * FROM newsletter ORDER BY date_time_subscribe DESC");
        	while ($n = $newsletter->fetch()) {
        ?>
        <li class="list__newsletter"><a class="link-article--primary" href="../php/main.php?email=<?= $n['email'] ?>"><i class="fas fa-times"></i></a><span><?= $n["email"] ?></span></li><?php } ?>
      </ul>
      <!-- dialog-->
      <dialog class="dialog">
        <div>
          <p>Voulez-vous vraiment supprimer cet article ?</p>
          <div class="dialog__menu"><a class="btn btn--accent" onclick="modal('close')"> <i class="fas fa-window-close"></i><span>Annuler</span></a><a class="btn btn--accent" href="" id="deleteLink"> <i class="fas fa-trash"></i><span>Supprimer</span></a></div>
        </div>
      </dialog>
    </section>
    <!-- Footer--><a id="contact"></a>
    <footer>
      <div class="social-footer__container">
        <div class="newsletter">
          <h2>Recevez la newsletter<br>de l'association !</h2>
          <form id="newsletterForm">
            <div class="form__container">
              <div class="field__container">
                <input type="email" name="email" disabled>
                <label class="field__label" for="email"><span>Adresse email</span></label>
              </div>
              <div class="newsletter__btn">
                <input id="newsletterButton" type="submit" value="DÉSACTIVÉ" disabled>
              </div>
            </div>
          </form>
        </div>
        <div class="contact">
          <h2>Contact</h2>
          <ul>
            <li><a class="btn-social--primary" href="mailto:safe-migrants-nantes@laposte.net" target="_blank" rel="noreferrer" aria-label="Email de Safe Migrants Nantes"><i class="fas fa-envelope fa-2x"></i></a></li>
            <li><a class="btn-social--primary" href="https://www.facebook.com/pg/CSPN44" target="_blank" rel="noreferrer" aria-label="Facebook de Safe Migrants Nantes"><i class="fab fa-facebook-square fa-2x"></i></a></li>
            <li><a class="btn-social--primary" href="https://www.messenger.com/t/CSPN44" target="_blank" rel="noreferrer" aria-label="Messenger de Safe Migrants Nantes"><i class="fab fa-facebook-messenger fa-2x"></i></a></li>
          </ul>
        </div>
      </div>
      <div class="mentions-legales">
        <h2>Mentions légales</h2>
        <div>
          <p>Un site réalisé avec ❤️ par <span class="highlight--primary">Ekkaia</span></p>
          <p>Crédit photo (page d'accueil) : <a class="link--primary highlight--primary" href="https://www.warrenrichardson.com" rel="noreferrer">Warren Richardson</a></p>
          <p>Consulter les mentions légales : <a class="link--primary highlight--primary" href="https://safe-migrants-nantes/mentions-legales">Mentions légales</a></p>
        </div>
      </div>
    </footer>
    <script src="../js/script.js"></script>
  </body>
</html>