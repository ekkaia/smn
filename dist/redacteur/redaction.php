<!DOCTYPE html>
<!--
███████╗ █████╗ ███████╗███████╗    ███╗   ███╗██╗ ██████╗ ██████╗  █████╗ ███╗   ██╗████████╗███████╗
██╔════╝██╔══██╗██╔════╝██╔════╝    ████╗ ████║██║██╔════╝ ██╔══██╗██╔══██╗████╗  ██║╚══██╔══╝██╔════╝
███████╗███████║█████╗  █████╗█████╗██╔████╔██║██║██║  ███╗██████╔╝███████║██╔██╗ ██║   ██║   ███████╗
╚════██║██╔══██║██╔══╝  ██╔══╝╚════╝██║╚██╔╝██║██║██║   ██║██╔══██╗██╔══██║██║╚██╗██║   ██║   ╚════██║
███████║██║  ██║██║     ███████╗    ██║ ╚═╝ ██║██║╚██████╔╝██║  ██║██║  ██║██║ ╚████║   ██║   ███████║
╚══════╝╚═╝  ╚═╝╚═╝     ╚══════╝    ╚═╝     ╚═╝╚═╝ ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═══╝   ╚═╝   ╚══════╝
-->
<html lang="fr" prefix="og: http://ogp.me/ns#">
  <head><?php include "../php/main.php" ?>
    <meta property="og:type" content="website">
    <title>Rédaction — Safe Migrants Nantes</title>
    <link rel="stylesheet" href="../stylesheet/style.css">
    <link rel="icon" type="image/png" href="../images/favicon.png">
    <meta charset="UTF-8">
    <meta name="theme-color" content="#f79f24">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- OGP-->
    <meta name="description" content="Page de rédaction des articles de Safe Migrants Nantes.">
    <meta property="og:site_name" content="Safe Migrants Nantes"><?php $url = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" ?>
    <meta property="og:url" content="<?= $url ?>">
    <meta property="og:title" content="Rédaction">
    <meta property="og:description" content="Page de rédaction des articles de Safe Migrants Nantes.">
    <meta property="og:image" content="https://safe-migrants-nantes.org/images/logo.jpg">
    <meta property="og:locale" content="fr_FR">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
  </head>
  <body>
    <!-- Navigation-->
    <header id="header">
      <div><a class="header__link" href="https://safe-migrants-nantes.org"><img class="header__logo" src="https://safe-migrants-nantes.org/images/logo.svg" alt="Logo de Safe Migrants Nantes">
          <p class="header__title highlight--primary">SAFE MIGRANTS NANTES</p></a></div>
      <input id="header__checkbox" type="checkbox">
      <label id="header__checkbox--label" for="header__checkbox"><i class="fas fa-bars fa-2x" id="header__checkbox--menu"></i></label>
      <nav>
        <ul>
          <li><a class="link--primary" href="https://safe-migrants-nantes.org/qui-sommes-nous">Qui sommes-nous ?</a></li>
          <li><a class="link--primary" href="https://safe-migrants-nantes.org/#nous-soutenir">Nous soutenir</a></li>
          <li><a class="link--primary" href="https://safe-migrants-nantes.org/liste-des-articles">Actualités</a></li>
          <li><a class="link--primary" href="https://safe-migrants-nantes.org#contact">Contact</a></li>
        </ul>
      </nav>
    </header>
    <!-- Background-header-->
    <div class="header__background background--pages"></div>
    <section class="redaction">
      <div class="alert"></div><?php 
      	if (isset($_GET["slug"])) {
      		$edit = true;	
      	} else {
      		$edit = false;	
      	}
      ?>
      <form class="form__container" id="redactionForm">
        <div class="field__container">
          <input type="text" name="titre" value="<?php if ($edit) { echo $titre; } ?>" required>
          <label class="field__label" for="titre"><span>Titre</span></label>
        </div>
        <div class="field__container">
          <textarea name="chapo" rows="5" required><?php if ($edit) { echo $chapo; } ?></textarea>
          <label class="field__label" for="chapo"><span>Chapo</span></label>
        </div>
        <div class="field__container">
          <input type="text" name="auteur" value="<?php if ($edit) { echo $auteur; } ?>" required>
          <label class="field__label" for="auteur"><span>Auteur</span></label>
        </div><?php if (!$edit) { ?>
        <div class="tag-picker">
          <label for="tag">Choisissez une catégorie :</label>
          <select name="tag">
            <option value="Article">Article</option>
            <option value="Chronique">Chronique</option>
            <option value="Évènement">Événement</option>
            <option value="Vie de l'association">Vie de l'association</option>
          </select>
        </div><?php } ?>
        <div class="btn-textarea__container">
          <button type="button" onclick="pushTextarea(`&lt;p&gt;&lt;/p&gt;`)"> <i class="fas fa-paragraph"></i><span>Paragraphe</span></button>
          <button type="button" onclick="pushTextarea(`&lt;h3 class='title--list'&gt;&lt;span&gt;&lt;/span&gt;&lt;/h3&gt;`)"><i class="fas fa-heading"> </i><span>Rubrique</span></button>
          <button type="button" onclick="pushTextarea(`&lt;blockquote&gt;&lt;p&gt;&lt;/p&gt;&lt;/blockquote&gt;`)"><i class="fas fa-quote-right"></i><span>Citation</span></button>
          <button type="button" onclick="pushTextarea(`&lt;a href='[Adresse du lien]' class='link-article--accent'&gt;&lt;/a&gt;`)"><i class="fas fa-link"></i><span>Lien</span></button>
          <button type="button" onclick="pushTextarea(`&lt;i&gt;&lt;/i&gt;`)"><i class="fas fa-italic"></i><span>Italique</span></button>
        </div>
        <div class="field__container">
          <textarea id="textarea" name="contenu" rows="10" required><?php if ($edit) { echo $contenu; } ?></textarea>
          <label class="field__label" for="contenu"><span>Contenu</span></label>
        </div><?php if (!$edit) { ?>
        <div class="img__uploader">
          <p>Votre image doit être au format .jpg. Préférez les images n'excedant pas 500 ko.</p>
          <label for="file" id="label"><i class="fas fa-upload"></i><span>Ajouter une image</span></label>
          <input type="file" id="file" name="miniature" onclick="fileInfo()">
          <div class="field__container" id="descImage">
            <input type="text" name="desc_image">
            <label class="field__label" for="desc_image"><span>Légende</span></label>
          </div>
        </div><?php } ?>
        <div class="redaction__submit">
          <input id="redactionButton" type="submit" value="PUBLIER L'ARTICLE">
        </div>
      </form>
    </section>
    <!-- Footer--><a id="contact"></a>
    <footer>
      <div class="social-footer__container">
        <div class="newsletter">
          <h2>Recevez la newsletter<br>de l'association !</h2>
          <form id="newsletterForm">
            <div class="form__container">
              <div class="field__container">
                <input type="email" name="email" disabled>
                <label class="field__label" for="email"><span>Adresse email</span></label>
              </div>
              <div class="newsletter__btn">
                <input id="newsletterButton" type="submit" value="DÉSACTIVÉ" disabled>
              </div>
            </div>
          </form>
        </div>
        <div class="contact">
          <h2>Contact</h2>
          <ul>
            <li><a class="btn-social--primary" href="mailto:safe-migrants-nantes@laposte.net" target="_blank" rel="noreferrer" aria-label="Email de Safe Migrants Nantes"><i class="fas fa-envelope fa-2x"></i></a></li>
            <li><a class="btn-social--primary" href="https://www.facebook.com/pg/CSPN44" target="_blank" rel="noreferrer" aria-label="Facebook de Safe Migrants Nantes"><i class="fab fa-facebook-square fa-2x"></i></a></li>
            <li><a class="btn-social--primary" href="https://www.messenger.com/t/CSPN44" target="_blank" rel="noreferrer" aria-label="Messenger de Safe Migrants Nantes"><i class="fab fa-facebook-messenger fa-2x"></i></a></li>
          </ul>
        </div>
      </div>
      <div class="mentions-legales">
        <h2>Mentions légales</h2>
        <div>
          <p>Un site réalisé avec ❤️ par <span class="highlight--primary">Ekkaia</span></p>
          <p>Crédit photo (page d'accueil) : <a class="link--primary highlight--primary" href="https://www.warrenrichardson.com" rel="noreferrer">Warren Richardson</a></p>
          <p>Consulter les mentions légales : <a class="link--primary highlight--primary" href="https://safe-migrants-nantes/mentions-legales">Mentions légales</a></p>
        </div>
      </div>
    </footer>
    <script src="../js/script.js"></script>
  </body>
</html>