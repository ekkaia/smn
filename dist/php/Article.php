<?php

class Article {
	public function __construct($bdd) {
		$this->bdd = $bdd ;
	}

	private function slugify($text) {
		$text = strip_tags($text);
		$text = preg_replace("~[^\pL\d]+~u", "-", $text);
		setlocale(LC_ALL, "en_US.utf8");
		$text = iconv("utf-8", "us-ascii//TRANSLIT", $text);
		$text = preg_replace("~[^-\w]+~", "", $text);
		$text = trim($text, "-");
		$text = preg_replace("~-+~", "-", $text);
		$text = strtolower($text);

		if (empty($text)) {
			return "n-a";
		}
	
		return $text;
	}

	public function create($titre, $auteur, $chapo, $tag, $contenu, $desc_image, $miniature) { // Miniature -> $_FILES["miniature"]
		if (!empty($titre)	AND !empty($chapo) AND !empty($auteur) AND !empty($tag) AND !empty($contenu)) {
			$contenu = strip_tags($contenu, "<p><a><span><h3><blockquote><cite><i><iframe>");
			$slug = $this->slugify($titre);

			if (isset($miniature) AND !empty($miniature["name"])) {
				if (exif_imagetype($miniature["tmp_name"]) == IMAGETYPE_JPEG) {
					$chemin = "../images/miniatures/" . $slug . ".jpg";
					move_uploaded_file($_FILES["miniature"]["tmp_name"], $chemin);
					
					if (!isset($desc_image) OR empty($desc_image)) {
						return "Vous n'avez pas légendé votre image !";
					}
				} else {
					return "Votre image doit être au format jpg !";
				}
			} else {
				$desc_image = NULL;
			}
			
			$ins = $this->bdd->prepare("INSERT INTO articles (titre, auteur, chapo, contenu, description_image, date_time_publication, slug, tag) VALUES (?, ?, ?, ?, ?, NOW(), ?, ?)");
			$ins->execute([$titre, $auteur, $chapo, $contenu, $desc_image, $slug, $tag]);
			return $slug;
		} else {
			return "Veuillez remplir tous les champs !";
		}
	}

	public function edit($titre, $auteur, $chapo, $contenu, $slug) {
		if (!empty($titre)	AND !empty($chapo) AND !empty($auteur) AND !empty($contenu) AND !empty($slug)) {
			$contenu = strip_tags($contenu, "<p><a><span><h3><blockquote><cite><i><iframe>");

			$findId = $this->bdd->prepare("SELECT id FROM articles WHERE slug = ?");
			$findId->execute([$slug]);
			$id = $findId->fetchColumn();

			$img = "../images/miniatures/" . $slug . ".jpg";
			$slug = $this->slugify($titre);

			if (file_exists($img)) {
				$rename = "../images/miniatures/" . $slug . ".jpg";
				rename($img, $rename);
			}

			$ins = $this->bdd->prepare("UPDATE articles SET titre = ?, auteur = ?, chapo = ?, contenu = ?, slug = ?, date_time_edition = NOW() WHERE id = ?");
			$ins->execute([$titre, $auteur, $chapo, $contenu, $slug, $id]);
			return $slug;
		}
	}

	public function delete($slug) {
		if (!empty($slug)) {
			$findId = $this->bdd->prepare("SELECT id FROM articles WHERE slug = ?");
			$findId->execute([$slug]);
			$id = $findId->fetchColumn();

			if ($id != false) {
				$delete = $this->bdd->prepare("DELETE FROM articles WHERE id = ?");
				$delete->execute([$id]);
				header("Location: ../redacteur/gestion.php?success=" . $slug);
			} else {
				header("Location: ../redacteur/gestion.php?error=" . $slug);
			}
		} else {
			header("Location: ../redacteur/gestion.php?error=" . $slug);
		}
	}

	public function display($slug) {
		if (!empty($slug)) {
			$findId = $this->bdd ->prepare("SELECT id FROM articles WHERE slug = ?");
			$findId->execute([$slug]);
			$id = $findId->fetchColumn();

			$article = $this->bdd ->prepare("SELECT * FROM articles WHERE id = ?");
			$article->execute([$id]);
			if ($article->rowCount() == 1) {
				return $article = $article->fetch();
			} else {
				die("Une erreur est survenue lors du chargement de cet article !");
			}
		} else {
			die("Erreur ! Cet article n'existe pas !");
		}
	}
}