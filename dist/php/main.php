<?php
require "autoload.php";
require "bdd.php";

// Abonnement email
if (isset($_POST["email"])) { // Email
	$newsletter = new Newsletter($bdd);
	echo $newsletter->subscribe($_POST["email"]);
}

// Suppression email 
if (isset($_GET["email"])) { // Email
	$newsletter = new Newsletter($bdd);
	echo $newsletter->unsubscribe($_GET["email"]);
}

// Suppression article
if (isset($_GET["delete"])) { // Slug
	$article = new Article($bdd);
	echo $article->delete($_GET["delete"]);
}

// Nouvel article
if (isset($_POST["titre"], $_POST["auteur"], $_POST["chapo"], $_POST["tag"], $_POST["contenu"], $_POST["desc_image"], $_FILES["miniature"])) {
	$article = new Article($bdd);
	echo $article->create($_POST["titre"], $_POST["auteur"], $_POST["chapo"], $_POST["tag"], $_POST["contenu"], $_POST["desc_image"], $_FILES["miniature"]);
}

// Edition d'un article
if (isset($_POST["titre"], $_POST["auteur"], $_POST["chapo"], $_POST["contenu"], $_POST["slug"])) {
	$article = new Article($bdd);
	echo $article->edit($_POST["titre"], $_POST["auteur"], $_POST["chapo"], $_POST["contenu"], $_POST["slug"]);

}

// Affichage article
if (isset($_GET["slug"])) {
	$article = new Article($bdd);
	$display = $article->display($_GET["slug"]);
	$titre = $display["titre"];
	$chapo = $display["chapo"];
	$auteur = $display["auteur"];
	$slug = $display["slug"];
	$tag = $display["tag"];
	$description_image = $display["description_image"];
	$contenu = $display["contenu"];
	$date_time_edition = $display["date_time_edition"];
	$date_time_publication = $display["date_time_publication"];
}

function displaySuccessMessage($success) {
	return "<div class='alert--success'><p><i class='fas fa-check'></i><span class='highlight--dark'> $success</span> a bien été supprimé !</p></div>";
}

function displayErrorMessage($error) {
	return "<div class='alert--error'><p><i class='fas fa-exclamation-triangle'></i> Une erreur est survenue : <span class='highlight--dark'>$error</span>.</p></div>";
}