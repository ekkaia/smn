<?php

class Newsletter {
	public function __construct($bdd) {
		$this->bdd = $bdd ;
	}

	public function subscribe($email) {
		if (!empty($email)) {
			if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
				$count = $this->bdd->prepare("SELECT COUNT(*) AS nb FROM newsletter WHERE email = ?");
				$count->execute([$email]);
				$result = $count->fetchColumn();
		
				if ($result == 0) {
					$ins = $this->bdd->prepare("INSERT INTO newsletter (email, date_time_subscribe) VALUES (?, NOW())");
					$ins->execute([$email]);
					$message = "VOUS ÊTES ABONNÉ !";
				} else {
					$message = "VOUS ÊTES DÉJÀ ABONNÉ !";
				}
			} else {
				$message = "EMAIL INCORRECT !";
			}
		} else {
			$message = "ERREUR !";
		}

		return $message;
	}

	public function unsubscribe($email) {
		if (!empty($email) AND (filter_var($email, FILTER_VALIDATE_EMAIL))) {
			$findId = $this->bdd->prepare("SELECT id FROM newsletter WHERE email = ?");
			$findId->execute([$email]);
			$id = $findId->fetchColumn();

			if ($id) {
				$delete = $this->bdd->prepare("DELETE FROM newsletter WHERE id = ?");
				$delete->execute([$id]);
				header("Location: ../redacteur/gestion.php?success=" . $email);
			} else {
				header("Location: ../redacteur/gestion.php?error=" . $email);
			}
		} else {
			header("Location: ../redacteur/gestion.php?error=" . $email);
		}
	}
}